package de.rwth.swc.sqa.glassbox;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringHelperExDTest {

    @Test
    void demonstrationTest() {
        assertEquals( "abc  ", StringHelper.stripFromStart("yxabc  ", "xyz"));
    }

    @ParameterizedTest(name = "Nr {0}, param = ({1}, {2}); expected = {3}")
    @CsvSource({
            "1, , , ",
            "2, 'a', '', 'a'",
            "3, 'a', 'a', ''",
            "4, ' a', , 'a'",
            "5, 'a', 'b', 'a'",
            "6, 'aa', 'a', ''",
            "7, 'a', , 'a'",
            "8, '  a', , 'a'"
    })
    void exerciseD(int testNumber, String str, String stripChars, String result) {
        assertEquals( result, StringHelper.stripFromStart(str, stripChars));
    }
}
